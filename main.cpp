#include "ui_listView.h"
#include <iostream>
#include <QMainWindow>
#include <QApplication>
#include <QPushButton>
#include <QSlider>
#include <QString>
#include <QStyleFactory>
#include <QUrl>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>


#ifdef __wasm__

#define APIURL "/radioapi"
#include <emscripten.h>
EM_JS(void, audioInit, (), {
    var player = document.createElement("AUDIO");
    player.id = "audioPlayer";
    player.hidden = true;
    document.body.appendChild(player);
});
EM_JS(void, setVolume, (int volume), {
    var player = document.getElementById("audioPlayer");
    player.volume = volume / 100;
});
EM_JS(void, pauseAudio, (), {
    var player = document.getElementById("audioPlayer");
    player.pause();
});
EM_JS(void, resumeAudio, (), {
    var player = document.getElementById("audioPlayer");
    player.play();
});
EM_JS(void, playFromUrlJs, (const char *url), {
    var player = document.getElementById("audioPlayer");
    player.pause();
    player.src = UTF8ToString(url);
    player.play();
});
inline void playFromUrl(QString url) {
    playFromUrlJs(url.toStdString().c_str());
}

#else

#define APIURL "https://prod.radio-api.net"
#include <QMediaPlayer>
static QMediaPlayer *player = nullptr;
inline void audioInit () {
    player = new QMediaPlayer;
}
inline void setVolume(int volume) {
    player->setVolume(volume);
}
inline void pauseAudio() {
    player->pause();
}
inline void resumeAudio() {
    player->play();
}
inline void playFromUrl(QString url) {
    player->setMedia(QUrl(url));
    player->play();
}

#endif


class _UILoader {
    QNetworkAccessManager *netManager = nullptr;
    QJsonArray *stationsCache = nullptr;

public:
    _UILoader() {
        netManager = new QNetworkAccessManager;
        stationsCache = new QJsonArray;
    }
    ~_UILoader() {
        delete netManager;
        delete stationsCache;
    }

    void MainWindow(QMainWindow *w) {
        Ui::listView *thisui = new Ui::listView;

        // Show initial UI
        thisui->setupUi(w);
        w->show();
        thisui->pauseButton->setHidden(true);
        thisui->resumeButton->setHidden(true);

        // Get list of local stations
        {
            // Create request
            QNetworkRequest request;
            request.setUrl(QUrl(APIURL "/stations/local?count=100"));

            // Connect list update
            w->connect(netManager, &QNetworkAccessManager::finished, [this, thisui] (QNetworkReply *reply) {
                const auto stations = QJsonDocument::fromJson(reply->readAll())["playables"].toArray();

                // Append stations to layout
                for (const auto& station : stations) {
                    thisui->stationList->addItem(station["name"].toString());
                    stationsCache->append(station);
                }
            });

            // Perform request
            netManager->get(request);
        }

        // Add button handlers
        w->connect(thisui->stationList, &QListWidget::itemSelectionChanged, [this, thisui] () {
            const auto station = stationsCache->at(thisui->stationList->row(thisui->stationList->selectedItems()[0]));
            const QString mediaUrl = station["streams"][0]["url"].toString();
            thisui->nowPlaying->setText("Now playing: " + station["name"].toString());
            thisui->pauseButton->setHidden(false);
            thisui->resumeButton->setHidden(true);
            playFromUrl(mediaUrl);
        });

        w->connect(thisui->volumeSlider, &QSlider::valueChanged, [thisui] () {
            setVolume(thisui->volumeSlider->value());
        });

        w->connect(thisui->pauseButton, &QPushButton::clicked, [thisui] () {
            pauseAudio();
            thisui->pauseButton->setHidden(true);
            thisui->resumeButton->setHidden(false);
        });

        w->connect(thisui->resumeButton, &QPushButton::clicked, [thisui] () {
            resumeAudio();
            thisui->pauseButton->setHidden(false);
            thisui->resumeButton->setHidden(true);
        });
    }
};


int main(int argc, char *argv[]) {

    //Initialise variables
    QApplication a(argc, argv);
    QMainWindow w;
    _UILoader UILoader;

    // Initliase theme
    //qApp->setStyle(QStyleFactory::create(*currentTheme));

    // Initialise audio
    audioInit();

    // Load initial window
    UILoader.MainWindow(&w);

    // Run forever
    return a.exec();
}
